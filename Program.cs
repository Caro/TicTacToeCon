﻿bool gameEnd = false;
int player = 1;


string[] arenaOriginal = {
    "1", "2", "3",
    "4", "5", "6",
    "7", "8", "9"
};

string[] arenaGame = new string[9];

// Main Program

NewGame();
do
{
    Console.Clear();
    Statement();
    PrintField();
    if (!ProofWin())
    {
        int intEingabe = Ask();
        if (intEingabe == 666)
        {
            Failure();
            break;
        }
        WriteInArray(intEingabe);
        if (intEingabe == 0)
            gameEnd = true;
    }
    
} while (!gameEnd);

// End Main Program

void Statement()
{
    Console.WriteLine("Player 1: X");
    Console.WriteLine("Player 2: O");
    Console.WriteLine();
}

void NewGame()
{
    for (int i = 0; i < arenaOriginal.Length; i++)
        arenaGame[i] = arenaOriginal[i];
    player = 1;
}

void Failure()
{
    Console.Clear();
    Console.WriteLine("Irgendetwas ist schief gelaufen...");
    Console.WriteLine("Mit beliebiger Taste weiter...");
    Console.ReadKey();
}

void PrintField()
{
    Console.WriteLine(" " + arenaGame[0] + " | " + arenaGame[1] + " | " + arenaGame[2]);
    Console.WriteLine("---+---+---");
    Console.WriteLine(" " + arenaGame[3] + " | " + arenaGame[4] + " | " + arenaGame[5]);
    Console.WriteLine("---+---+---");
    Console.WriteLine(" " + arenaGame[6] + " | " + arenaGame[7] + " | " + arenaGame[8]);
}

int Ask()
{
    string strEingabe;

    Console.WriteLine();
    Console.Write("Wähle eine Zahl Player {0}: ", player);
    strEingabe = Console.ReadLine();

    if (int.TryParse(strEingabe, out int result) && result > 0 && result < 10)
    {
        if (result == 0)
        {
            Environment.Exit(0);
        }

        return result;
    }
    return 666;
}

void WriteInArray(int i)
{
    bool player1 = player == 1;
    bool player2 = player == 2;
    bool placeEmpty = (arenaGame[i - 1] != "X") && (arenaGame[i - 1] != "O");

    if (player1 && placeEmpty)
    {
        arenaGame[i - 1] = "X";
        player = 2;
    }

    if (player2 && placeEmpty)
    {
        arenaGame[i - 1] = "O";
        player = 1;
    }
}

bool ProofWin()
{
    bool row;
    bool column;
    bool cross;
    bool draw;
    int countDraw = 9;

    for (int i = 0; i < 9; i++)
    {
        if (!int.TryParse(arenaGame[i], out int result))
        {
            countDraw--;
        }
    }

    if (countDraw == 0)
    {
        draw = true;
    }
    else
    {
        draw = false;
    }

    row = (string.Equals(arenaGame[0], arenaGame[1]) && string.Equals(arenaGame[1], arenaGame[2])) ||
          (string.Equals(arenaGame[3], arenaGame[4]) && string.Equals(arenaGame[4], arenaGame[5])) ||
          (string.Equals(arenaGame[6], arenaGame[7]) && string.Equals(arenaGame[7], arenaGame[8]));

    column = (string.Equals(arenaGame[0], arenaGame[3]) && string.Equals(arenaGame[3], arenaGame[6])) ||
             (string.Equals(arenaGame[1], arenaGame[4]) && string.Equals(arenaGame[4], arenaGame[7])) ||
             (string.Equals(arenaGame[2], arenaGame[5]) && string.Equals(arenaGame[5], arenaGame[8]));

    cross = (string.Equals(arenaGame[0], arenaGame[4]) && string.Equals(arenaGame[4], arenaGame[8])) ||
            (string.Equals(arenaGame[2], arenaGame[4]) && string.Equals(arenaGame[4], arenaGame[6]));

    if (row || column || cross)
    {
        Console.Clear();
        PrintField();
        Console.WriteLine();
        Console.WriteLine("Herzlichen Glückwunsch!");

        if (player != 1)
            Console.WriteLine("Spieler 1 hat gewonnen.");

        if (player != 2)
            Console.WriteLine("Spieler 2 hat gewonnen.");

        if (Close())
            return true;
    }

    if (draw)
    {
        Console.Clear();
        PrintField();
        Console.WriteLine();
        Console.WriteLine("Unentschieden!");
        System.Console.WriteLine("Beide Spieler teilen sich den Gewinn!");
        if (Close())
            return true;
    }

    return false;
}

bool Close()
{
    Console.WriteLine();
    Console.Write("(N)eues Spiel oder (B)eenden? ");
    string strInput = Console.ReadLine();
    if (strInput.Equals("n") || strInput.Equals("N"))
    {
        NewGame();
        return true;
    }
    
    if (strInput.Equals("b") || strInput.Equals("B"))
        Environment.Exit(0);
    return false;
}